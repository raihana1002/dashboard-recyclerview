package com.raihana_10191068.dash;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DashAdapter extends RecyclerView.Adapter<DashAdapter.DashHolder>{

    private ArrayList<SetterGetter> listdata;

    public DashAdapter(ArrayList<SetterGetter> listdata){
        this.listdata = listdata;
    }
    @NonNull
    @Override
    public DashHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view           = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dash, parent, false);
        DashHolder holder   = new DashHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DashHolder holder, int position) {

        final SetterGetter getData  = listdata.get(position);
        String titlemenu = getData.getTitle();
        String logomenu = getData.getImg();

        holder.titleMenu.setText(titlemenu);
        if(logomenu.equals("logomenu1")){
            holder.imgmenu.setImageResource(R.drawable.news);
        } else if(logomenu.equals("logomenu2")){
            holder.imgmenu.setImageResource(R.drawable.clock);
        }else if(logomenu.equals("logomenu3")){
            holder.imgmenu.setImageResource(R.drawable.facebook);
        }else if(logomenu.equals("logomenu4")){
            holder.imgmenu.setImageResource(R.drawable.twitter);
        }else if(logomenu.equals("logomenu5")){
            holder.imgmenu.setImageResource(R.drawable.maps);
        }else if(logomenu.equals("logomenu6")){
            holder.imgmenu.setImageResource(R.drawable.youtube);
        }else if(logomenu.equals("logomenu7")){
            holder.imgmenu.setImageResource(R.drawable.calculator);
        }else if(logomenu.equals("logomenu8")){
            holder.imgmenu.setImageResource(R.drawable.globe);
        }else if(logomenu.equals("logomenu9")){
            holder.imgmenu.setImageResource(R.drawable.compass);
        }else if(logomenu.equals("logomenu10")){
            holder.imgmenu.setImageResource(R.drawable.shop);
        }else if(logomenu.equals("logomenu11")){
            holder.imgmenu.setImageResource(R.drawable.bubbles);
        }else if(logomenu.equals("logomenu12")){
            holder.imgmenu.setImageResource(R.drawable.calendar);
        }else if(logomenu.equals("logomenu13")){
            holder.imgmenu.setImageResource(R.drawable.gamepad);
        }else if(logomenu.equals("logomenu14")){
            holder.imgmenu.setImageResource(R.drawable.headphone);
        }else if(logomenu.equals("logomenu15")){
            holder.imgmenu.setImageResource(R.drawable.gallery);
        }

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class DashHolder extends RecyclerView.ViewHolder {

        TextView titleMenu;
        ImageView imgmenu;

        public DashHolder(@NonNull View itemView) {
            super(itemView);

            titleMenu   = itemView.findViewById(R.id.tv_title_menu);
            imgmenu     = itemView.findViewById(R.id.iv_menu);
        }
    }
}

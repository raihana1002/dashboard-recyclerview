package com.raihana_10191068.dash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.GridLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> datamenu;
    GridLayoutManager gridLayoutManager;
    DashAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rv_menu);

        addData();
        gridLayoutManager   = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashAdapter(datamenu);
        recyclerView.setAdapter(adapter);

    }

    public void addData(){

        datamenu = new ArrayList<>();
        datamenu.add(new SetterGetter("News", "logomenu1"));
        datamenu.add(new SetterGetter("Clock", "logomenu2"));
        datamenu.add(new SetterGetter("Facebook", "logomenu3"));
        datamenu.add(new SetterGetter("Twitter", "logomenu4"));
        datamenu.add(new SetterGetter("Maps", "logomenu5"));
        datamenu.add(new SetterGetter("Youtube", "logomenu6"));
        datamenu.add(new SetterGetter("Calculator", "logomenu7"));
        datamenu.add(new SetterGetter("Dunia", "logomenu8"));
        datamenu.add(new SetterGetter("Kompas", "logomenu9"));
        datamenu.add(new SetterGetter("Shop", "logomenu10"));
        datamenu.add(new SetterGetter("Message", "logomenu11"));
        datamenu.add(new SetterGetter("Calender", "logomenu12"));
        datamenu.add(new SetterGetter("Game", "logomenu13"));
        datamenu.add(new SetterGetter("Musik", "logomenu14"));
        datamenu.add(new SetterGetter("Gallery", "logomenu15"));





    }
}